# imports
from quantopian.algorithm import attach_pipeline, pipeline_output
from quantopian.pipeline import Pipeline
from quantopian.pipeline.data.builtin import USEquityPricing
from quantopian.pipeline.factors import Latest, Returns
from quantopian.pipeline.filters.morningstar import IsPrimaryShare
from quantopian.pipeline.data import morningstar
import talib as ta
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from quantopian.algorithm import order_optimal_portfolio
import quantopian.optimize as opt
from quantopian.pipeline.data.psychsignal import aggregated_twitter_withretweets_stocktwits as st

def initialize(context):
    
    attach_pipeline(make_pipeline(context), 'my_algorithm') 
    schedule_function(order_positions, date_rules.every_day(), time_rules.market_open(minutes=15))
    schedule_function(evaluate, date_rules.every_day(), time_rules.market_open())
    schedule_function(sell, date_rules.every_day(), time_rules.market_open(minutes=1))
    schedule_function(log_positions, date_rules.every_day(), time_rules.market_open(hours=1))
    set_long_only()

    context.age={}
    context.max_age = 1
    context.total_stops = 0
    context.total_profits = 0
    context.total_age_orders = 0
    context.total_rsi = 0

def make_pipeline(context):
   
    price = USEquityPricing.close.latest
    atmost_price = price < 5
    atleast_price = price > 1
    primary_share = IsPrimaryShare()
    common_stock = morningstar.share_class_reference.security_type.latest.eq('ST00000001')
    have_market_cap = morningstar.valuation.market_cap.latest.notnull()

    tradeable_stocks = (
        primary_share
        & common_stock
        & have_market_cap
        & atmost_price
        & atleast_price
    )

    recent_returns = Returns(inputs=[USEquityPricing.close], window_length=5)
    top_returns = recent_returns > 0.08

    volume = USEquityPricing.volume.latest
    low_vol = volume < 500000
    
    bull_sentiment = st.bull_scored_messages.latest
    bear_sentiment = st.bear_scored_messages.latest
    bull_pos = bull_sentiment > bear_sentiment
    
    # Filter
    securitties_to_trade = (top_returns & atmost_price & low_vol & atleast_price & bull_pos & tradeable_stocks) 

    # Create Pipeling
    pipe = Pipeline( 
        columns={
            'volume': volume,
            'bull_sentiment': bull_sentiment,
            'bear_sentiment': bear_sentiment
        },
        screen=(securitties_to_trade) # screen securities to trade
    )
    return pipe

# Register output before market opens
def before_trading_start(context, data):
	output = pipeline_output('my_algorithm')
	security_list = output.index.tolist()
	context.securities_in_results = []
    context.longs = []

	for sec in security_list:

		volume = data.history(sec, 'volume', 25, '1d')
	    
		max15_vol = 0
		max5_vol = 0
		
        for i in range(0, 25):
			if (volume[i] > max15_vol):
				max15_vol = volume[i]
		for j in range(20, 25):
			if (volume[j] > max5_vol):
				max5_vol = volume[j]
		
        if (max15_vol > 950000 and max5_vol < 600000):
			if sec not in context.securities_in_results:
				context.securities_in_results.append(sec)

# for our filtered penny stocks, try to predict if the next day will go up
# using a random forest classifier
def evaluate (context, data):
    if len(context.securities_in_results) > 0.0:                    
        for sec in context.securities_in_results:
           
            recent_prices, recent_volumes = data.history(sec, 'price', context.history_range, '1d').values, 
            data.history(sec, 'volume', context.history_range, '1d').values
            price_changes, volume_changes = np.diff(recent_prices).tolist(), np.diff(recent_volumes).tolist()
            X,Y = [],[]
            
            for i in range(context.history_range-context.lookback-1): 
                X.append(price_changes[i:i+context.lookback] + volume_changes[i:i+context.lookback])
                Y.append(price_changes[i+context.lookback])
                
            context.model.fit(X, Y) 
            recent_prices, recent_volumes = data.history(sec, 'price', context.lookback+1, '1d').values, 
            data.history(sec, 'volume', context.lookback+1, '1d').values
            price_changes, volume_changes = np.diff(recent_prices).tolist(), np.diff(recent_volumes).tolist()
            prediction = context.model.predict(price_changes + volume_changes) # was price_changes + volume_changes
            
            if prediction > -0.5: 
                print(str(sec.symbol) +  " | " + str(prediction))
                if sec not in context.portfolio.positions:
                    context.longs.append(sec)

# order the optimal amount of shares
def order_positions(context, data):
	weights = {}
	for sec in context.longs:

        if sec in context.age:
    		context.age[sec] += 1
    	else:
    		context.age[sec] = 1

    	if data.can_trade(sec):
    		weights[sec] = (1.0 / len(context.longs))
    		order_optimal_portfolio(
        		opt.TargetWeights(weights), 
        		constraints=[
                opt.LongOnly(sec),
                ],
                )  

# see if we should sell stocks held
def sell(context, data):
	record(stops=context.total_stops, profits=context.total_profits, age_orders=context.total_age_orders, rsi_orders=context.total_rsi)
	for sec in context.portfolio.positions:
		cost_basis = context.portfolio.positions[sec].cost_basis
		context.current_position = context.portfolio.positions[sec].amount
		price_current = data.current(sec, 'price')
		price_hist_15 = data.history(sec, 'price', 15, '1d')
		rsi = ta.RSI(price_hist_15, timeperiod=14)[-1]
		
		if context.age[sec] > context.max_age:
			log.info( str(sec) + ' Sold security, max age')
			context.total_age_orders += 1
			order_target_percent(sec, 0)
		
		elif price_current <= cost_basis * 0.96 and context.current_position > 0:  
            log.info( str(sec) + ' Sold security, stop loss')
            context.total_stops += 1
            order_target_percent(sec, 0)

        elif price_current > cost_basis * 1.1 and context.current_position > 0:
        	log.info( str(sec) + ' Sold security, take profit')
        	context.total_profits += 1
        	order_target_percent(sec, 0)

        elif rsi > 80 and context.current_position > 0:
        	log.info( str(sec) + ' Sold security, high rsi')
        	context.total_rsi += 1
        	order_target_percent(sec, 0)


def log_positions(context, data):
    # Log all held positions  
    if len(context.portfolio.positions) > 0:  
        all_positions = "Current positions for %s : " % (str(get_datetime()))  
        for pos in context.portfolio.positions:  
            if context.portfolio.positions[pos].amount != 0:  
                all_positions += "%s at %s shares \n" %\
                (pos.symbol, context.portfolio.positions[pos].amount)  
        log.info(all_positions)





