Synopsis
-----
Algorithm can be deployed or backtested at quantopian.com  

Algorithm description
---------
The algorithm seeks to find penny stocks that have performed exceedingly  
well in the short term while maintaining low volume. For these   
filtered stocks, try to predict the next day's price and volume movement  
using a random forest classifier. If the model suggests an upward move,  
buy the stock in hope that buying pressure increase and selling  
pressure will decrease.
