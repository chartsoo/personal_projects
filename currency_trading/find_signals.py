#!/usr/bin/python
# sub program to find buying opportunities

# imports
import numpy as np
import math
from utility_funcs import NATR, OBV

def find_signal(Account, Trade, Frame):
	
	# find slope in order to determine diveregence
	# compute slope value for all ranges length-rg
	# -----------------------------------------------------------------------------
	def find_sig(high, low, close, OBV_in, pair, frame):
		
		# normalize obv in order to filter min slope
		maxv = max(OBV_in)
		minv = min(OBV_in)
		sc = max(abs(minv), maxv)
		OBV = [(x+sc)/sc for x in OBV_in]

		for rg in range(28, 60):
			begin = (len(close) - rg)
			end = len(close)
	
			# slice arrays by interval rg to length
			pr = np.array(close[begin:end])
			obv = np.array(OBV[begin:end])
			
			if trough(pr):
				
				p_slope = slope(pr)
				o_slope = slope(obv)

				# we want some volatility
				level = volatility(frame, high, low, close)
				cycle_change = swing(close)

				# This is the filter for possible trades:
				if (o_slope > 0) and (p_slope < 0) and (obv[0] < obv[-1]) and (cycle_change > 1.06):
					
					# store pair/frame and relevant statistics to see if we want to buy later
					Account.possible_trade(pair, frame, o_slope, p_slope, obv[-1], cycle_change, rg)
					return True, 1, 1

		return False, 0, 0 # did not find a divergence

	# compute the slope using np.polyfit with order 1
	def slope(y):
		x = range(0, len(y))
		coeffs = np.polyfit(x, y, 1)

		intercept = coeffs[-1]  # might use this in ordering weight
		slope = coeffs[-2]
		power = 0 # since order is 1

		return slope

	# see if it is volatile enough to trust divergence
	def volatility(frame, high, low, close):
		natr = NATR(high, low, close)
		
		hard_val = 0

		if frame=="H1":
			hard_val = 0.12
		elif frame=="H4":
			hard_val = 0.18
		elif frame=="D":
			hard_val = 0.7

		return (natr[-1] > hard_val)

	# look at the percent difference between high and low price in interval
	def swing(close):
		MAX = max(close)
		MIN = min(close)

		diff = MAX - MIN
		delta = (diff / MIN * 100)

		return delta

	# determine of price is in a trough for given interval
	def trough(close):
		mn = min(close)
		return (mn==close[-1])

	# execution starts here
	# -----------------------------------------------------------------------------
	# oanda account
	access_token = Account.access_token
	client = Account.client

	# list of current assets
	orders = 0

	# iterate through all forex pairs
	pair_list = ['EUR_USD', 'EUR_GBP', 'USD_CHF', 'GBP_USD', 'USD_JPY', 
		'AUD_USD', 'USD_CAD', 'EUR_JPY', 'EUR_NZD', 'USD_HKD', 'EUR_SGD', 
		'AUD_CAD', 'AUD_CHF', 'AUD_NZD', 'CAD_CHF', 'CAD_JPY', 'CHF_JPY', 
		'EUR_AUD', 'EUR_CAD', 'EUR_CHF', 'GBP_HKD']

	#print("< frame:", Frame)
	vol = []
	close = []
	for pair in pair_list:

		# get api data
		time, vol, op, high, low, close = Account.get_data(Frame, pair)

		if close.size == 0:
			break
		
		# get statistics
		obv = OBV(close, vol)

		# find slopes and divergencies
		find_sig(high, low, close, obv, pair, Frame)











