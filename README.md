This reopository contains some of my personal projects as well as  
some course projects.

Projects include:
-----------------------------------------------------------------------------

- Automated currency trading: (personal)  
- Quantitative stock trading: (personal)  
- Client server program: (cmps 109)  
- File storage utility: (cmps 109)  
- Makefile interpretation program: (cmps 112)  