Description
-----------------------------------------------------------------------
Program mimics the unix hierchic file sytem. Data is stored in a  
balanced tree with interior nodes representing directories and leaves  
representing files.

Usage
-----------------------------------------------------------------------

**commands:** 
  
cat  
cd  
echo  
exit  
ls  
make    
mkdir    
prompt      
pwd  
rm  